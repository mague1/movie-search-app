# Semantic Web Search with AlloyDB vector search

## Overview

This lab will walk users through building a semantic search web application for searching through movie plot summaries using AlloyDB vector search to find movies and similar movies.

## Workflow

Users will:

1. Create a new database
1. Populate the database with pre-embedded data
1. Test the search functionality on the command line
1. Run a web application and search data in real time
1. Benchmark various index types to see which is most performant



## Getting Started

### Open a shell on the provisioned virtual machine

In the Google Cloud Console open up your [VM Instances](https://console.cloud.google.com/compute/instances)

Click on the SSH button and select `Open in browser window` 

![image](images/open_vm_ssh.png)

### Change to the application user


![image](images/sudo_cmd.png)

```sh
sudo su - ubuntu
```


### Export the necessary ENV vars

The necessary environment setting can be found on the lab output section

![image](images/labvars.png)

```
export PGUSER=alloyuser
export PGPASSWORD=XXXXXXXXXXXXXXXXXXXXXXX
export PGHOST=XX.XX.XX.XX
export PGPORT=5432
export PGDB=movies
```

### Create a New Database

First we need to create the movies database.

connect

```sh
psql -U ${PGUSER} -h ${PGHOST} -p ${PGPORT} postgres
```

create
```sql
create database movies;
```

disconnect
```sql
\q
```

### Populate the database with Pre-embedded Data

Load the pre-vectorized data into the database.

#### Change to the application directory

```sh
cd movie_search/
```

The raw movie data is stored in `movies.csv` and the pre-vectorized data is stored in `embeddings.json`

#### View sample data

```sh
cat embeddings.json |jq .[0] -r | head -30
```

#### Load the data

As running the vectorization process takes a considerable amount of time we will load a pre-vectorized version.  See `gen_fast_load_file.py` for how we pre-vectorize.

Please be patient as this takes a few minutes

```sh
python3 fast_load_data.py 
```

### Test the search functionality on the command line

To confirm that we have loaded the data correctly, we can run a command line query to search for movies.

As this creates initial connections and starts up the model it is not very fast, you will see better performance with the application.

```
python3 query.py Samurai kills a bunch of bad guys
```

You should see results like 
```
Searching for movies that match: samurai kills a bunch of bad guys
1. Jûbê ninpûchô
2. Ninja Scroll
3. Jûbei ninpûchô: Ryuhogyoku-hen
4. Alien vs. Ninja
5. Versus
```

#### Look at the script

`query.py` takes the user input and vectorizes the data using the open source model specified
and then runs the database query using SQLAlchemy


###  Run a web application and search data in real time
#### Start the application

Spin up the web based application for movie search.

```sh
python3 app.py 
```

#### Open in the web browser

Copy the URL from the Lab session outputs and open in any web browser.

```
http://XX.XX.XX.XX:5000
```

Search for a movie by adding a short prompt on the plot of the movies and find similar movies based on plot.

Click on Simlar in the results to find movies that are close to this one.

Click on Explain in the top left for infomation on how the database is structured.

### Performance Testing

We will run a performance test against the database by running through each movie and finding the 5 closest results.  This simulates a larger load on the database and we can make some changes to see the effects on our test.

#### Run the performance test

Get a baseline performance with no indexing of the vectorized summaries.

```sh
python3 performance_test.py 
```

#### Create an HNSW index

Build an index with HNSW.

connect to the database

```sh
psql -U ${PGUSER} -h ${PGHOST} -p ${PGPORT} ${PGDB}
```

turn on timing

```sql
\timing on
```

create the HNSW index

```sql
create index on embeddings using hnsw (embedding vector_cosine_ops);
```

Note the time it takes to create the index

```
CREATE INDEX
Time: 9073.372 ms (00:09.073)
```

disconnect from the database

```sql
movies=> \q
```

Re-run the benchmark to see a performance improvement.
You can see the operations per second much higher than the benchmark without the index

```sh
python3 performance_test.py 
```

#### Create an IVFFlat index

Try building another kind of index - note the index creation time.

connect to the database

```sh
psql -U ${PGUSER} -h ${PGHOST} -p ${PGPORT} ${PGDB}
```

turn on timing

```sql
\timing on
```

View current indexes

```sql
SELECT indexname, indexdef FROM pg_indexes WHERE tablename='embeddings';
```

Drop the HNSW index we created

```sql
drop index embeddings_embedding_idx;
```

Replace it with an IVFFlat index

```sql
create index on embeddings using ivfflat (embedding vector_cosine_ops);
```

Note that this index was created much faster

View current indexes

```sql
SELECT indexname, indexdef FROM pg_indexes WHERE tablename='embeddings';
```

disconnect from the database

```sql
\q
```

Re-run the benchmark to see a performance impacts.

```sh
python3 performance_test.py 
```

The index types are a trade off between index creation time and query throughput.  Note that the results may change on larger datasets to where one may suddenly become more performative.  Keep this in mind when building your own application.

## Further Reading

### PGVector
- [PGVector](https://github.com/pgvector/pgvector)

### Code Sample
- [Movie Search Application](https://gitlab.com/mague1/movie-search-app)

### Indexing
- [HNSW Tutorial](https://github.com/brtholomy/hnsw)
- [IVFFlat](https://github.com/pgvector/pgvector?tab=readme-ov-file#ivfflat)
- [HNSWW](https://github.com/pgvector/pgvector?tab=readme-ov-file#hnsw)
