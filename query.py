#!/usr/bin/env python

import sys
from os import getenv

from pgvector.sqlalchemy import Vector
from sentence_transformers import SentenceTransformer
from sqlalchemy import create_engine, select, text, Integer, String, Text
from sqlalchemy.orm import declarative_base, mapped_column, Session

import warnings
warnings.filterwarnings("ignore")

# Build our connection string from the environment variables
PG_USER = getenv("PGUSER", "postgres")
PG_PASSWORD = getenv("PGPASSWORD", "vectorVICTOR")
PG_HOST = getenv("PGHOST", "localhost")
PG_PORT = getenv("PGPORT", "5432")
PG_DB = getenv("PGDB", "movies")

engine = create_engine('postgresql+psycopg://{}:{}@{}:{}/{}'.format(PG_USER, PG_PASSWORD, PG_HOST, PG_PORT, PG_DB))


# set a default search string
search_string = "samurai kills a bunch of bad guys"

# If the user supplies a search string then go ahead and join it all into one sentence
if len(sys.argv) > 1:
    search_string = ' '.join(sys.argv[1:])

# Set up the classes for the sqlalchemy ORM
Base = declarative_base()

class Embeds(Base):
    __tablename__ = "embeddings"

    id = mapped_column(Integer, primary_key=True)
    embedding = mapped_column(Vector(384))        # This is the column that stores the actual vectors
    movie_id = mapped_column(Integer)


class Movie(Base):
    __tablename__ = "movies"

    id = mapped_column(Integer, primary_key=True)
    imdb_id = mapped_column(String(12))
    title = mapped_column(Text)
    synopsis = mapped_column(Text)

# Use the following AI model 
model = SentenceTransformer('all-MiniLM-L6-v2')
# Take our search string and run it through the mode to generate a Vector of length 384 that we will use to search
e = model.encode(search_string)

session = Session(engine)
# Find me the 5 closest neighbors by cosine distance 
neighbors = session.scalars(select(Embeds).order_by(Embeds.embedding.cosine_distance(e)).limit(5))
print("Searching for movies that match: {}".format(search_string))
count = 1
for neighbor in neighbors:
        mv = session.get(Movie, neighbor.movie_id)
        print("{}. {}".format(count, mv.title))
        count += 1
