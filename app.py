from os import getenv
from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap
from pgvector.sqlalchemy import Vector
from sentence_transformers import SentenceTransformer
from sqlalchemy import create_engine, select, text, Integer, String, Text
from sqlalchemy.orm import declarative_base, mapped_column, Session


import warnings
warnings.filterwarnings("ignore")

app = Flask(
    __name__,
    static_url_path="/docs",
    static_folder="docs",
)

bootstrap = Bootstrap()
Base = declarative_base()

PG_USER = getenv("PGUSER", "postgres")
PG_PASSWORD = getenv("PGPASSWORD", "vectorVICTOR")
PG_HOST = getenv("PGHOST", "localhost")
PG_PORT = getenv("PGPORT", "5432")
PG_DB = getenv("PGDB", "movies")
engine = create_engine('postgresql+psycopg://{}:{}@{}:{}/{}'.format(PG_USER, PG_PASSWORD, PG_HOST, PG_PORT, PG_DB))


class Embeds(Base):
    __tablename__ = "embeddings"

    id = mapped_column(Integer, primary_key=True)
    embedding = mapped_column(Vector(384))
    movie_id = mapped_column(Integer)


class Movie(Base):
    __tablename__ = "movies"

    id = mapped_column(Integer, primary_key=True)
    imdb_id = mapped_column(String(12))
    title = mapped_column(Text)
    synopsis = mapped_column(Text)
    tags = mapped_column(Text)


def movie_search(search_string):
    matches = []
    model = SentenceTransformer("all-MiniLM-L6-v2")
    e = model.encode(search_string)
    session = Session(engine)
    neighbors = session.scalars(
        select(Embeds).order_by(Embeds.embedding.cosine_distance(e)).limit(5)
    )
    for neighbor in neighbors:
        mv = session.get(Movie, neighbor.movie_id)
        matches.append(mv)

    return matches


def get_title(movie_id):
    title = ""
    session = Session(engine)
    stmt = select(Movie).where(Movie.id == movie_id)
    for e in session.scalars(stmt):
        title = e.title

    return title


def movie_match(movie_id):
    matches = []
    session = Session(engine)
    stmt = select(Embeds).where(Embeds.movie_id == movie_id)
    for e in session.scalars(stmt):
        neighbors = session.scalars(
            select(Embeds).where(Embeds.movie_id != movie_id)
            .order_by(Embeds.embedding.cosine_distance(e.embedding))
            .limit(5)
        )
        for neighbor in neighbors:
            mv = session.get(Movie, neighbor.movie_id)
            matches.append(mv)

    return matches


@app.route("/")
def index():
    return render_template("search.html")


@app.route("/explain")
def show_explain():
    return render_template("how.html")


@app.route("/display", methods=["POST"])
def display():
    req = request.form["searchtxt"]
    matches = movie_search(req)
    return render_template("results.html", req=req, matches=matches)


@app.route("/similar")
def similar():
    mid = int(request.args.get("movie_id"))
    req = "Movies similar to - "
    req += get_title(mid)
    matches = movie_match(mid)
    return render_template("results.html", req=req, matches=matches)


if __name__ == "__main__":
    bootstrap.init_app(app)
    app.debug = True
    app.run(port=5000, host="0.0.0.0")
