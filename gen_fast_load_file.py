#!/usr/bin/env python

import csv
import json
from sentence_transformers import SentenceTransformer

model = SentenceTransformer('all-MiniLM-L6-v2')


import warnings
warnings.filterwarnings("ignore")

movies = []

with open("movies.csv", "r") as f:
    reader = csv.DictReader(f)
    for row in reader:
        row['embedding'] = model.encode(row['plot_synopsis']).tolist()
        movies.append(row)

with open("embeddings.json", "w") as f:
    json.dump(movies, f)
