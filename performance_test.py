#!/usr/bin/env python

import time
import numpy as np
from os import getenv

from alive_progress import alive_bar
from pgvector.sqlalchemy import Vector
from sqlalchemy import create_engine, select, text, Integer, String, Text
from sqlalchemy.orm import declarative_base, mapped_column, Session


# Get the connection string from the environment variable
PG_USER = getenv("PGUSER", "postgres")
PG_PASSWORD = getenv("PGPASSWORD", "vectorVICTOR")
PG_HOST = getenv("PGHOST", "localhost")
PG_PORT = getenv("PGPORT", "5432")
PG_DB = getenv("PGDB", "movies")

# Connect to the database
engine = create_engine('postgresql+psycopg://{}:{}@{}:{}/{}'.format(PG_USER, PG_PASSWORD, PG_HOST, PG_PORT, PG_DB))


Base = declarative_base()

# The following classes map the two tables into types for the Sqlalchemy ORM
class Embeds(Base):
    __tablename__ = "embeddings"

    id = mapped_column(Integer, primary_key=True)
    embedding = mapped_column(Vector(384))        # This sets a vector column in the database that we will query
    movie_id = mapped_column(Integer)


class Movie(Base):
    __tablename__ = "movies"

    id = mapped_column(Integer, primary_key=True)
    imdb_id = mapped_column(String(12))
    title = mapped_column(Text)
    synopsis = mapped_column(Text)

# We will store our performance statistics in this array
stats = np.array([])

# Hit the database
session = Session(engine)
# Get a list of all of the movies in the database
movies = session.query(Movie).all()
with alive_bar(len(movies)) as bar:
    # For each movie loop through
    for x in movies:
        embed_search  = session.scalars(select(Embeds).where(Embeds.movie_id == x.id)).first()
        t1 = time.time()
        # find me the closest 5 movies in a vector search
        neighbors = session.scalars(select(Embeds).order_by(Embeds.embedding.cosine_distance(embed_search.embedding)).limit(5))
        t2 = time.time()
        stats = np.append(stats, t2 - t1)
        bar()

# Print out our statistics
print("========================================================================")
print("mean", stats.mean())    
print("p95", np.percentile(stats, 95))
print("p99", np.percentile(stats, 99))
