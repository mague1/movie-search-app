#!/usr/bin/env python

# Take the pregenerated embeddings.json file and load it into the db as fast as possible

import json
from os import getenv
from pgvector.sqlalchemy import Vector
from sqlalchemy import create_engine, insert, select, text, Integer, String, Text
from sqlalchemy.orm import declarative_base, mapped_column, Session
from sqlalchemy.dialects.postgresql import ARRAY
from alive_progress import alive_bar

PG_USER = getenv("PGUSER", "postgres")
PG_PASSWORD = getenv("PGPASSWORD", "vectorVICTOR")
PG_HOST = getenv("PGHOST", "localhost")
PG_PORT = getenv("PGPORT", "5432")
PG_DB = getenv("PGDB", "movies")

engine = create_engine('postgresql+psycopg://{}:{}@{}:{}/{}'.format(PG_USER, PG_PASSWORD, PG_HOST, PG_PORT, PG_DB))
with engine.connect() as conn:
    conn.execute(text("CREATE EXTENSION IF NOT EXISTS vector"))
    conn.commit()

Base = declarative_base()


class Embeds(Base):
    __tablename__ = "embeddings"

    id = mapped_column(Integer, primary_key=True)
    embedding = mapped_column(Vector(384))
    movie_id = mapped_column(Integer)


class Movie(Base):
    __tablename__ = "movies"

    id = mapped_column(Integer, primary_key=True)
    imdb_id = mapped_column(String(12))
    title = mapped_column(Text)
    synopsis = mapped_column(Text)
    tags = mapped_column(Text)


Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)
session = Session(engine)

f = open("embeddings.json")
data = json.load(f)
batch = 0
with alive_bar(len(data)) as bar:
    for row in data:
        session.execute(insert(Embeds), [dict(movie_id=row['id'], embedding=row['embedding'])])
        session.execute(
            insert(Movie),
            dict(imdb_id=row['imdb_id'], id=row['id'], title=row['title'], synopsis=row['plot_synopsis'], tags=row['tags']),
        )
        if batch >0 and batch % 100 == 0:
            session.commit()
        batch += 1
        bar()
    
session.commit()
